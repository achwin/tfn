<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;

class FuzzyController extends Controller
{
	private $totalA;
    private $totalC;
    private $totalD;

    public function index()
    {
    	return view('index');
    }

    public function init(Request $request)
    {
    	$attributes = $request->all();
    	// Initiate variable A C D
    	$this->totalA = (int) $attributes['totalA'];
    	$this->totalC = (int) $attributes['totalC'];
    	$this->totalD = (int) $attributes['totalD'];

    	// Storing excel

    	// Criteria
    	$fileCriteria = $request->file('criteria');
    	$fileNameCriteria = time().'-criteria.'.$fileCriteria->getClientOriginalExtension();
    	$fileCriteria->move(public_path('excel/'),$fileNameCriteria);

    	// Bobot
    	$fileBobot = $request->file('bobot');
    	$fileNameBobot = time().'-bobot.'.$fileBobot->getClientOriginalExtension();
    	$fileBobot->move(public_path('excel/'),$fileNameBobot);

    	// Rating
    	$fileRating = $request->file('rating');
    	$fileNameRating = time().'-rating.'.$fileRating->getClientOriginalExtension();
    	$fileRating->move(public_path('excel/'),$fileNameRating);

    	$excelBobot = Excel::load('excel/'.$fileNameBobot)->get();
    	$bobot = [];
    	foreach ($excelBobot as $key => $result) {
    		for ($d=1; $d <= $this->totalD; $d++) { 
    			$bobot[$result[0]]['D'.$d] = $result['d'.$d];
    		}
    	}
    	
    	$excelRating = Excel::load('excel/'.$fileNameRating)->get();
    	$c = 1;
    	foreach ($excelRating as $key => $result) {
    		for ($d=1; $d <= $this->totalD ; $d++) { 
    			$rating['C'.$c][$result[0]]['D'.$d] = $result['d'.$d];
    		}
    		if ($result[0] == 'A'.$this->totalA) {
    			$c++;
    		}
    	}

    	$excelCriteria = Excel::load('excel/'.$fileNameCriteria)->get();
    	foreach ($excelCriteria as $key => $result) {
    		$criteria[$result['criteria']] = $result['type'];
    	}

    $bobot = $this->bobotToTFN($bobot);
    $rating = $this->ratingToTFN($rating);
    $decision = $this->ratingToDecision($rating,$bobot);
    $normalizedAndWeighted = $this->normalizeAndWeight($decision,$criteria);
    if ($attributes['cara'] == '1') {
    	$solution = $this->solution($normalizedAndWeighted);
    }
    elseif ($attributes['cara'] == '2') {
    	$solution = $this->solution2($normalizedAndWeighted);
    }
    $euclidPlus = $this->euclid($normalizedAndWeighted,$solution,'*');
    $euclidMinus = $this->euclid($normalizedAndWeighted,$solution,'-');
    $cityBlockPlus = $this->cityBlock($normalizedAndWeighted,$solution,'*');
    $cityBlockMinus = $this->cityBlock($normalizedAndWeighted,$solution,'-');
    $chebyshevPlus = $this->chebyshev($normalizedAndWeighted,$solution,'*');
    $chebyshevMinus = $this->chebyshev($normalizedAndWeighted,$solution,'-');
    $preferenceEuclid = $this->preference($euclidPlus,$euclidMinus);
    $preferenceCityBlock = $this->preference($cityBlockPlus,$cityBlockMinus);
    $preferenceChebysev = $this->preference($chebyshevPlus,$chebyshevMinus);
    $data = [
    	'totalA'				=> $this->totalA,
    	'totalC'				=> $this->totalC,
    	'totalD'				=> $this->totalD,
    	'decision'				=> $decision,
    	'normalizedAndWeighted' => $normalizedAndWeighted,
    	'solution'				=> $solution,
    	'euclidPlus'			=> $euclidPlus,
    	'euclidMinus'			=> $euclidMinus,
    	'cityBlockPlus'			=> $cityBlockPlus,
    	'cityBlockMinus'		=> $cityBlockMinus,
    	'chebyshevPlus'			=> $chebyshevPlus,
    	'chebyshevMinus'		=> $chebyshevMinus,
    	'preferenceEuclid'		=> $preferenceEuclid,
    	'preferenceCityBlock'	=> $preferenceCityBlock,
    	'preferenceChebysev'	=> $preferenceChebysev,
    	'cara'					=> $attributes['cara'],
    ];
    return view('result',$data);
    }

    // Konversi bobot linguistik ke TFN
    public function bobotToTFN($bobot)
    {
    	$converted = [];
    	foreach ($bobot as $key1 => $c) {
    		foreach ($c as $key2 => $d) {
    			switch ($d) {
    				case 'VL':
    					$c[$key2] = [0,0,0.1,0.2];
    					break;
    				case 'L':
    					$c[$key2] = [0.1,0.2,0.2,0.3];
    					break;
    				case 'ML':
    					$c[$key2] = [0.2,0.3,0.4,0.5];
    					break;
    				case 'M':
    					$c[$key2] = [0.4,0.4,0.5,0.6];
    					break;
    				case 'MH':
    					$c[$key2] = [0.5,0.6,0.7,0.8];
    					break;
    				case 'H':
    					$c[$key2] = [0.7,0.8,0.8,0.9];
    					break;
    				case 'VH':
    					$c[$key2] = [0.8,0.9,1,1];
    					break;
    				default:
    					break;
    			}
    		}
 		$converted[$key1] = $c;	
    	}
    	return $converted;
    }

    // Konversi rating linguistik ke TFN
    public function ratingToTFN($rating)
    {
    	$converted = [];
    	foreach ($rating as $key1 => $c) {
    		foreach ($c as $key2 => $a) {
    			foreach ($a as $key3 => $d) {
    				switch ($d) {
    					case 'VP':
    						$c[$key2][$key3] = [0,0,1,2];
    						break;
    					case 'P':
    						$c[$key2][$key3] = [1,2,2,3];
    						break;
    					case 'MP':
    						$c[$key2][$key3] = [2,3,4,5];
    						break;
    					case 'F':
    						$c[$key2][$key3] = [4,5,5,6];
    						break;
    					case 'MG':
    						$c[$key2][$key3] = [5,6,7,8];
    						break;
    					case 'G':
    						$c[$key2][$key3] = [7,8,8,9];
    						break;
    					case 'VG':
    						$c[$key2][$key3] = [8,9,10,10];
    						break;
    					default:
    						# code...
    						break;
    				}
    			}
    		}
    		$converted[$key1] = $c;
    	}
    	return $converted;
    }

    // Konversi rating ke matriks keputusan
    public function ratingToDecision($rating,$bobot)
    {
    	$decision = [];
    	// dd($rating);
    	for ($i=1; $i <= $this->totalA; $i++) { 
    		for ($j=1; $j <= $this->totalC ; $j++) { 
    			for ($k=0; $k < 4; $k++) { 
    				switch ($k) {
    					case 0:
    						$min = $rating['C'.$j]['A'.$i]['D1'][0];
    						for ($d=1; $d <= $this->totalD ; $d++) { 
    							if ($rating['C'.$j]['A'.$i]['D'.$d][0] < $min) {
    								$min = $rating['C'.$j]['A'.$i]['D'.$d][0];
    							}
    						}
    						$decision['A'.$i]['C'.$j][$k] = $min;
    						break;
    					case 1:
    						$sumD = 0;
    						for ($d=1; $d <= $this->totalD ; $d++) { 
    							$sumD = $sumD + $rating['C'.$j]['A'.$i]['D'.$d][1];
    						}
    						$decision['A'.$i]['C'.$j][$k] = $sumD/$this->totalD;
    						break;
    					case 2:
    						$sumD = 0;
    						for ($d=1; $d <= $this->totalD ; $d++) { 
    							$sumD = $sumD + $rating['C'.$j]['A'.$i]['D'.$d][2];
    						}
    						$decision['A'.$i]['C'.$j][$k] = $sumD/$this->totalD;
    						break;
    					case 3:
    						$max = $rating['C'.$j]['A'.$i]['D1'][3];
    						for ($d=1; $d <= $this->totalD ; $d++) { 
    							if ($rating['C'.$j]['A'.$i]['D'.$d][3] > $max) {
    								$max = $rating['C'.$j]['A'.$i]['D'.$d][3];
    							}
    						}
    						$decision['A'.$i]['C'.$j][$k] = $max;
    						break;
    					default:
    						# code...
    						break;
    				}
    			}
    		}
    	}
    	for ($i=1; $i <= $this->totalC; $i++) { 
    		for ($j=0; $j < 4; $j++) { 
    			switch ($j) {
    				case 0:
    					$min = $bobot['C'.$i]['D1'][0];
    					for ($d=1; $d <= $this->totalD; $d++) { 
    						if ($bobot['C'.$i]['D'.$d][0] < $min) {
    							$min = $bobot['C'.$i]['D'.$d][0];
    						}
    					}
    					$decision['weight']['C'.$i][$j] = $min;
    					break;
    				case 1:
    					$sumBobotD = 0;
    					for ($d=1; $d <= $this->totalD ; $d++) { 
    						$sumBobotD = $sumBobotD + $bobot['C'.$i]['D'.$d][1];
    					}
    					$decision['weight']['C'.$i][$j] = $sumBobotD/$this->totalD;
    					break;
    				case 2:
    					$sumBobotD = 0;
    					for ($d=1; $d <= $this->totalD ; $d++) { 
    						$sumBobotD = $sumBobotD + $bobot['C'.$i]['D'.$d][2];
    					}
    					$decision['weight']['C'.$i][$j] = $sumBobotD/$this->totalD;
    					break;
    				case 3:
    					$max = $bobot['C'.$i]['D1'][3];
    					for ($d=1; $d <= $this->totalD; $d++) { 
    						if ($bobot['C'.$i]['D'.$d][3] > $max) {
    							$max = $bobot['C'.$i]['D'.$d][3];
    						}
    					}
    					$decision['weight']['C'.$i][$j] = $max;
    					break;
    				default:
    					# code...
    					break;
    			}
    		}
    	}
    	return $decision;
    }

    // Matriks keputusan ternormalisasi
    public function normalizeAndWeight($decision,$criteria)
    {
    	$dij = [];
    	$normalizedAndWeighted = [];
    	for ($c=1; $c <= $this->totalC; $c++) { 
    		if ($criteria['C'.$c] == 'benefit') {
    			$max = $decision['A1']['C'.$c][3];
	    		for ($a=1; $a <= $this->totalA; $a++) { 
	    			if ($decision['A'.$a]['C'.$c][3] > $max) {
	    				$max = $decision['A'.$a]['C'.$c][3];
	    			}
	    		}
	    		$dij['C'.$c] = $max;
    		}
    		elseif ($criteria['C'.$c] == 'cost') {
    			$min = $decision['A1']['C'.$c][0];
	    		for ($a=1; $a <= $this->totalA; $a++) { 
	    			if ($decision['A'.$a]['C'.$c][0] < $min) {
	    				$min = $decision['A'.$a]['C'.$c][0];
	    			}
	    		}
	    		$dij['C'.$c] = $min;	
    		}
    	}
    	foreach ($decision as $key1 => $a) {
    		foreach ($a as $key2 => $c) {
    			if ($criteria[$key2] == 'benefit') {
    				for ($i=0; $i < 4; $i++) { 
    				$normalizedAndWeighted[$key1][$key2][$i] = 
    					($decision[$key1][$key2][$i]/$dij[$key2])*$decision['weight'][$key2][$i];
    				}
    			}
    			elseif ($criteria[$key2] == 'cost') {
    				for ($i=0; $i < 4; $i++) { 
    				$normalizedAndWeighted[$key1][$key2][$i] = 
    					($dij[$key2]/$decision[$key1][$key2][$i]);
    				}
    				sort($normalizedAndWeighted[$key1][$key2]);
    				for ($i=0; $i < 4; $i++) { 
    					$normalizedAndWeighted[$key1][$key2][$i] = $normalizedAndWeighted[$key1][$key2][$i]*$decision['weight'][$key2][$i];
    				}
    			}
    		}
    	}
    	return $normalizedAndWeighted;
    }

    // Matriks solusi a* a-
    public function solution($normalizedAndWeighted)
    {
    	$solution = [];
    	$maxC = [];
    	$minC = [];

    	// Mencari nilai terbesar tiap Criteria
    	for ($c=1; $c <= $this->totalC; $c++) { 
    		$max = 0;
    		for ($a=1; $a <= $this->totalA ; $a++) { 
    			for ($i=0; $i < 4; $i++) { 
    				if ($normalizedAndWeighted['A'.$a]['C'.$c][$i] > $max) {
    					$max = $normalizedAndWeighted['A'.$a]['C'.$c][$i];
    				}
    			}
    		}
    		$maxC['C'.$c] = $max;
    	}
		
		// Mencari nilai terkecil tiap Criteria    	
    	for ($c=1; $c <= $this->totalC; $c++) { 
    		$min = $normalizedAndWeighted['A1']['C'.$c][0];
    		for ($a=1; $a <= $this->totalA ; $a++) { 
    			for ($i=0; $i < 4; $i++) { 
    				if ($normalizedAndWeighted['A'.$a]['C'.$c][$i] < $min) {
    					$min = $normalizedAndWeighted['A'.$a]['C'.$c][$i];
    				}
    			}
    		}
    		$minC['C'.$c] = $min;
    	}

    	for ($a=0; $a < 2; $a++) { 
    		switch ($a) {
    			case 0:
    				for ($c=1; $c <= $this->totalC ; $c++) { 
    					for ($i=0; $i < 4; $i++) { 
    						$solution['A*']['C'.$c][$i] = $maxC['C'.$c];
    					}
    				}
    				break;
    			case 1:
    				for ($c=1; $c <= $this->totalC ; $c++) { 
    					for ($i=0; $i < 4; $i++) { 
    						$solution['A-']['C'.$c][$i] = $minC['C'.$c];
    					}
    				}
    				break;
    			default:
    				# code...
    				break;
    		}
    	}
    	return $solution;
    }

    // Solusi v.2
    public function solution2($normalizedAndWeighted)
    {
    	$solution = [];
    	$maxC = [];
    	$minC = [];

    	// Mencari nilai terbesar tiap Criteria
    	for ($c=1; $c <= $this->totalC; $c++) { 
    		for ($a=1; $a <= $this->totalA ; $a++) { 
    			$maxC['C'.$c]['A'.$a] = $normalizedAndWeighted['A'.$a]['C'.$c][3];
    		}
    		sort($maxC['C'.$c]);
    	}
    	for ($a=1; $a <= $this->totalA; $a++) { 
    		for ($c=1; $c <= $this->totalC; $c++) { 
    			for ($i=0; $i < 4; $i++) { 
    				if ($normalizedAndWeighted['A'.$a]['C'.$c][$i] > min($maxC['C'.$c])) {
    					$maxC['C'.$c][0] = $normalizedAndWeighted['A'.$a]['C'.$c][$i];
    					sort($maxC['C'.$c]);
    				}
    			}
    		}
    	}

        for ($c=1; $c <= $this->totalC; $c++) { 
            rsort($maxC['C'.$c]);
        }
    	for ($c=1; $c <= $this->totalC; $c++) { 
    		for ($i=0; $i < $this->totalA; $i++) { 
    			$newMaxC['C'.$c][$i] = $maxC['C'.$c][$i];
    		}
            sort($newMaxC['C'.$c]);
    	}
		// Mencari nilai terkecil tiap Criteria    	
    	for ($c=1; $c <= $this->totalC; $c++) { 
    		for ($a=1; $a <= $this->totalA ; $a++) { 
    			$minC['C'.$c]['A'.$a] = $normalizedAndWeighted['A'.$a]['C'.$c][0];
    		}
    		sort($minC['C'.$c]);
    	}
    	
    	for ($a=1; $a <= $this->totalA; $a++) { 
    		for ($c=1; $c <= $this->totalC; $c++) { 
    			for ($i=1; $i < 4; $i++) { 
    				if ($normalizedAndWeighted['A'.$a]['C'.$c][$i] < max($minC['C'.$c])) {
    					$minC['C'.$c][4] = $normalizedAndWeighted['A'.$a]['C'.$c][$i];
    					sort($minC['C'.$c]);
    				}
    			}
    		}
    	}
    	for ($c=1; $c <= $this->totalC; $c++) { 
    		for ($i=0; $i < $this->totalA; $i++) { 
    			$newMinC['C'.$c][$i] = $minC['C'.$c][$i];
    		}
    	}
    	for ($a=0; $a < 2; $a++) { 
    		switch ($a) {
    			case 0:
    				for ($c=1; $c <= $this->totalC ; $c++) { 
    					for ($i=0; $i < 4; $i++) { 
                            if (isset($newMaxC['C'.$c][$i])) {
    						  $solution['A*']['C'.$c][$i] = $newMaxC['C'.$c][$i];
                            }
                            else{
                              $solution['A*']['C'.$c][$i] = $newMaxC['C'.$c][$this->totalA - 1];
                            }
    					}
    				}
    				break;
    			case 1:
    				for ($c=1; $c <= $this->totalC ; $c++) { 
    					for ($i=0; $i < 4; $i++) { 
                            if (isset($newMinC['C'.$c][$i])) {
    						     $solution['A-']['C'.$c][$i] = $newMinC['C'.$c][$i];
                            }
                            else{
                                 $solution['A-']['C'.$c][$i] = $newMinC['C'.$c][$this->totalA - 1];
                            }
    					}
    				}
    				break;
    			default:
    				# code...
    				break;
    		}
    	}
    	return $solution;
    }

    // Menghitung matriks euclid A*
    public function euclid($normalizedAndWeighted,$solution,$type)
    {
    	$euclidA = [];
    	for ($a=1; $a <= $this->totalA; $a++) { 
    		for ($c=1; $c <= $this->totalC; $c++) { 
				$euclid = 0;
				for ($j=0; $j < 4; $j++) { 
					$euclid = $euclid + pow(($normalizedAndWeighted['A'.$a]['C'.$c][$j]-$solution['A'.$type]['C'.$c][$j]), 2);
				}
				$euclidA['d(A'.$a.',A'.$type.')']['C'.$c] = 
				sqrt($euclid/4);
    		}
    	}
    	return $euclidA;
    }

    // Cityblock distance
    public function cityBlock($normalizedAndWeighted,$solution,$type)
    {
    	$euclidCityBlock = [];
    	for ($a=1; $a <= $this->totalA; $a++) { 
    		for ($c=1; $c <= $this->totalC; $c++) { 
				$euclid = 0;
				for ($j=0; $j < 4; $j++) { 
					$euclid = $euclid + abs($normalizedAndWeighted['A'.$a]['C'.$c][$j]-$solution['A'.$type]['C'.$c][$j]);
				}
				$euclidCityBlock['d(A'.$a.',A'.$type.')']['C'.$c] = $euclid;
    		}
    	}
    	return $euclidCityBlock;
    }

    // Chebyshev distance
    public function chebyshev($normalizedAndWeighted,$solution,$type)
    {
    	$euclidChebyshev = [];
    	for ($a=1; $a <= $this->totalA; $a++) { 
    		for ($c=1; $c <= $this->totalC; $c++) { 
				$max = 0;
				for ($j=0; $j < 4; $j++) { 
					if (abs($normalizedAndWeighted['A'.$a]['C'.$c][$j]-$solution['A'.$type]['C'.$c][$j]) > $max) {
						$max = abs($normalizedAndWeighted['A'.$a]['C'.$c][$j]-$solution['A'.$type]['C'.$c][$j]);
					}
				}
				$euclidChebyshev['d(A'.$a.',A'.$type.')']['C'.$c] = $max;
    		}
    	}
    	return $euclidChebyshev;
    }

    // Nilai preferensi
    public function preference($plus,$minus)
    {
    	$preference = [];
    	for ($a=1; $a <= $this->totalA; $a++) { 
    		for ($i=0; $i < 4; $i++) { 
    			switch ($i) {
    				case 0:
    					$sumC = 0;
    					for ($c=1; $c <= $this->totalC; $c++) { 
    						$sumC = $sumC + 
    						$plus['d(A'.$a.',A*)']['C'.$c];
    					}
    					$preference['A'.$a]['d*'] = $sumC;
    					break;
    				case 1:
    					$sumC = 0;
    					for ($c=1; $c <= $this->totalC; $c++) { 
    						$sumC = $sumC + 
    						$minus['d(A'.$a.',A-)']['C'.$c];
    					}
    					$preference['A'.$a]['d-'] = $sumC;
    					break;
    				case 2:
    					$preference['A'.$a]['d*+i-'] = $preference['A'.$a]['d*'] + $preference['A'.$a]['d-'];
    					break;
    				case 3:
    					$preference['A'.$a]['Cci'] = $preference['A'.$a]['d-'] / $preference['A'.$a]['d*+i-'];
    					break;
    				default:
    					# code...
    					break;
    			}
    		}
    	}
    	$cci = [];
    	for ($a=1; $a <= $this->totalA; $a++) { 
    		array_push($cci, $preference['A'.$a]['Cci']);
    	}
    	rsort($cci);
    	for ($a=1; $a <= $this->totalA; $a++) { 
    		$preference['A'.$a]['Rank'] = array_search($preference['A'.$a]['Cci'], $cci) + 1;
    	}
    	return $preference;
    }
}
