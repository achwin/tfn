<html lang="en">
<head>
  <title>TFN</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="{{asset('css/datepicker.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script src="{{asset('js/datepicker.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClpt0QcdXEJYjkft0qCbWRtp7t3ff8Gwk&libraries=places&callback=initAutocomplete" async defer></script>
</head>
<body>

  <div class="container">
    <h2>TFN</h2>
    <form class="form-horizontal" method="POST" action="{{url('execute')}}" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Upload excel criteria:</label>
        <div class="col-md-10">
          <input type="file" name="criteria" class="form-control" class="form-control" accept=".xls,.xlsx">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Upload excel bobot:</label>
        <div class="col-md-10">
          <input type="file" name="bobot" class="form-control" accept=".xls,.xlsx">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Upload excel rating:</label>
        <div class="col-md-10">
          <input type="file" name="rating" class="form-control" accept=".xls,.xlsx">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Jumlah alternatif:</label>
        <div class="col-md-10">
          <input type="text" name="totalA" placeholder="Masukkan jumlah alternatif" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8 || key == 43);" class="form-control">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Jumlah criteria:</label>
        <div class="col-md-10">
          <input type="text" name="totalC" placeholder="Masukkan jumlah criteria" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8 || key == 43);" class="form-control">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Jumlah decision:</label>
        <div class="col-md-10">
          <input type="text" name="totalD" placeholder="Masukkan jumlah decision" onkeypress="var key = event.keyCode || event.charCode; return ((key  >= 48 && key  <= 57) || key == 8 || key == 43);" class="form-control">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Cara:</label>
        <div class="col-md-10">
          <select name="cara" class="form-control">
            <option value="1">1</option>
            <option value="2">2</option>
          </select>
        </div>
      </div>
      <div class="form-group">        
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
    </form>
  </div>

</body>
</html>