<!DOCTYPE html>
<html lang="en">
<head>
  <title>Result</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h3>Matriks keputusan</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        @for($c = 1;$c <= $totalC; $c++)
          <th colspan="4" style="text-align: center;">C{{$c}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>A{{$a}}</td>
        @for($c = 1;$c <= $totalC;$c++)
          @for($i = 0;$i < 4;$i++)
            <td>{{round($decision['A'.$a]['C'.$c][$i],3)}}</td>
          @endfor
        @endfor
      </tr>
      @endfor
      <tr>
        <td>Weight</td>
      @for($c = 1;$c <= $totalC;$c++)
        @for($i = 0;$i < 4;$i++)
          <td>{{round($decision['weight']['C'.$c][$i],3)}}</td>
        @endfor
      @endfor
      </tr>
    </tbody>
  </table>
  <br>
  <h3>Matriks keputusan ternomalisasi terbobot</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        @for($c = 1;$c <= $totalC; $c++)
          <th colspan="4" style="text-align: center;">C{{$c}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>A{{$a}}</td>
        @for($c = 1;$c <= $totalC;$c++)
          @for($i = 0;$i < 4;$i++)
            <td>{{round($normalizedAndWeighted['A'.$a]['C'.$c][$i],3)}}</td>
          @endfor
        @endfor
      </tr>
      @endfor
    </tbody>
  </table>
  <br>
  <h3>Matriks solusi ideal positif (A*) dan matriks solusi ideal negatif (A-) Cara {{$cara}}</h3>
    <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        @for($c = 1;$c <= $totalC; $c++)
          <th colspan="4" style="text-align: center;">C{{$c}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>A<sup>*</sup></td>
        @for($c = 1;$c <= $totalC;$c++)
          @for($i = 0;$i < 4;$i++)
            <td>{{round($solution['A*']['C'.$c][$i],3)}}</td>
          @endfor
        @endfor
      </tr>
      <tr>
        <td>A<sup>-</sup></td>
        @for($c = 1;$c <= $totalC;$c++)
          @for($i = 0;$i < 4;$i++)
            <td>{{round($solution['A-']['C'.$c][$i],3)}}</td>
          @endfor
        @endfor
      </tr>
    </tbody>
  </table>
  <br>
  <h3>Menghitung jarak A*</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        @for($c = 1;$c <= $totalC; $c++)
          <th style="text-align: center;">C{{$c}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>d(A{{$a}},A<sup>*</sup>)</td>
        @for($c = 1;$c <= $totalC;$c++)
            <td style="text-align: center;">{{round($euclidPlus['d(A'.$a.',A*)']['C'.$c],3)}}</td>
        @endfor
      </tr>
      @endfor
    </tbody>
  </table>
    <br>
  <h3>City Block A*</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        @for($c = 1;$c <= $totalC; $c++)
          <th style="text-align: center;">C{{$c}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>d(A{{$a}},A<sup>*</sup>)</td>
        @for($c = 1;$c <= $totalC;$c++)
            <td style="text-align: center;">{{round($cityBlockPlus['d(A'.$a.',A*)']['C'.$c],3)}}</td>
        @endfor
      </tr>
      @endfor
    </tbody>
  </table>
      <br>
  <h3>Chebysev A*</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        @for($c = 1;$c <= $totalC; $c++)
          <th style="text-align: center;">C{{$c}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>d(A{{$a}},A<sup>*</sup>)</td>
        @for($c = 1;$c <= $totalC;$c++)
            <td style="text-align: center;">{{round($chebyshevPlus['d(A'.$a.',A*)']['C'.$c],3)}}</td>
        @endfor
      </tr>
      @endfor
    </tbody>
  </table>
  <h3>Menghitung jarak A-</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        @for($c = 1;$c <= $totalC; $c++)
          <th style="text-align: center;">C{{$c}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>d(A{{$a}},A-)</td>
        @for($c = 1;$c <= $totalC;$c++)
            <td style="text-align: center;">{{round($euclidMinus['d(A'.$a.',A-)']['C'.$c],3)}}</td>
        @endfor
      </tr>
      @endfor
    </tbody>
  </table>
    <br>
  <h3>City Block A-</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        @for($c = 1;$c <= $totalC; $c++)
          <th style="text-align: center;">C{{$c}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>d(A{{$a}},A-)</td>
        @for($c = 1;$c <= $totalC;$c++)
            <td style="text-align: center;">{{round($cityBlockMinus['d(A'.$a.',A-)']['C'.$c],3)}}</td>
        @endfor
      </tr>
      @endfor
    </tbody>
  </table>
      <br>
  <h3>Chebysev A-</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        @for($c = 1;$c <= $totalC; $c++)
          <th style="text-align: center;">C{{$c}}</th>
        @endfor
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>d(A{{$a}},A-)</td>
        @for($c = 1;$c <= $totalC;$c++)
            <td style="text-align: center;">{{round($chebyshevMinus['d(A'.$a.',A-)']['C'.$c],3)}}</td>
        @endfor
      </tr>
      @endfor
    </tbody>
  </table>
  <br>
  <h3>Menghitung nilai preferensi</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        <th>d<sup>*</sup></th>
        <th>d-</th>
        <th>d<sup>*</sup>+i-</th>
        <th>Cci</th>
        <th>Rank</th>
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>A{{$a}}</td>
        <td>{{round($preferenceEuclid['A'.$a]['d*'],3)}}</td>
        <td>{{round($preferenceEuclid['A'.$a]['d-'],3)}}</td>
        <td>{{round($preferenceEuclid['A'.$a]['d*+i-'],3)}}</td>
        <td>{{round($preferenceEuclid['A'.$a]['Cci'],3)}}</td>
        <td>{{round($preferenceEuclid['A'.$a]['Rank'],3)}}</td>
      </tr>
      @endfor
    </tbody>
  </table>
  <br>
  <h3>Menghitung nilai preferensi Cityblock</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        <th>d<sup>*</sup></th>
        <th>d-</th>
        <th>d<sup>*</sup>+i-</th>
        <th>Cci</th>
        <th>Rank</th>
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>A{{$a}}</td>
        <td>{{round($preferenceCityBlock['A'.$a]['d*'],3)}}</td>
        <td>{{round($preferenceCityBlock['A'.$a]['d-'],3)}}</td>
        <td>{{round($preferenceCityBlock['A'.$a]['d*+i-'],3)}}</td>
        <td>{{round($preferenceCityBlock['A'.$a]['Cci'],3)}}</td>
        <td>{{round($preferenceCityBlock['A'.$a]['Rank'],3)}}</td>
      </tr>
      @endfor
    </tbody>
  </table>
  <br>
  <h3>Menghitung nilai preferensi Chebyshev</h3>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        <th>d<sup>*</sup></th>
        <th>d-</th>
        <th>d<sup>*</sup>+i-</th>
        <th>Cci</th>
        <th>Rank</th>
      </tr>
    </thead>
    <tbody>
      @for($a = 1;$a <= $totalA;$a++)
      <tr>
        <td>A{{$a}}</td>
        <td>{{round($preferenceChebysev['A'.$a]['d*'],3)}}</td>
        <td>{{round($preferenceChebysev['A'.$a]['d-'],3)}}</td>
        <td>{{round($preferenceChebysev['A'.$a]['d*+i-'],3)}}</td>
        <td>{{round($preferenceChebysev['A'.$a]['Cci'],3)}}</td>
        <td>{{round($preferenceChebysev['A'.$a]['Rank'],3)}}</td>
      </tr>
      @endfor
    </tbody>
  </table>
</div>

</body>
</html>
